import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import axios from "axios";

// axios.defaults.baseURL = "https://api.example.com"; //配置基础路径，`baseURL` 将自动加在 `url` 前面，除非 `url` 是一个绝对 URL。
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";
// 将axios对象，挂载到vue实例上，组件直接通过this就可以调用axios了。
Vue.prototype.axios = axios;

// 创建总线对象，空的vue对象，挂载到Vue.prototype，这样的话，每一个组件都可以通过this来访问这个总线对象
Vue.prototype.busvue = new Vue();
Vue.config.productionTip = false;

import BaiduMap from 'vue-baidu-map'
 
Vue.use(BaiduMap, {
  // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
  ak: 'u6Fdo3lo67pLWxE8BEdyD0TBltmGVFSu'
})

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
