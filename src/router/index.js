import VueRouter from 'vue-router'
import Vue from 'vue'
import jiaxiao from '@/views/jiaxiao.vue'
Vue.use(VueRouter)
import city from '@/components/city.vue'
const arrcity = () => import('@/views/arrcity.vue')
const citycode = () => import('@/components/citycode')
const dizhi = () => import('@/components/dizhisou')
const find = () => import('@/components/findname.vue')
const jiaxiang = () => import('@/views/jiaxiang.vue')
const baidu =()=> import('@/components/baidumap.vue')
const quanbu = () => import('@/views/quanbu.vue')
const xiangqing = () => import('@/views/xingqing.vue')
export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'jiaxiao',
      component: jiaxiao,
    },
    {
      path: '/arrcity',
      name: 'arrcity',
      component: arrcity,
      children: [{
        path: 'city',
        name: 'city',
        component: citycode,
      },
      {
        path: 'dizhi',
        name: 'dizhi',
        component: dizhi,
      },{
        path: 'find',
        name: 'find',
        component: find,
      }
    ],

    },
    {
      path: '/jiaxiang',
      name: 'jiaxiang',
      component: jiaxiang,
    },
    {
      path: '/baidu',
      name: 'baidu',
      component: baidu,
    },
    {
      path: '/quanbu',
      name: 'quanbu',
      component: quanbu,
    },
    {
      path: '/xiangqing',
      name: 'xiangqing',
      component: xiangqing,
    },
  ],
})
